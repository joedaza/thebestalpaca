<?php
/**
 * The template for displaying the footer.
 *
 * Contains the closing of the #content div and all content after
 *
 * @package 8Store Lite
 */

?>

</div><!-- #content -->
<div class="clear"></div>


<footer id="colophon" class="site-footer" role="contentinfo">



	<?php
	//Top Footer Widget
	if(is_active_sidebar('footer-1')){
		?>

		<section id="section-footer" class="clear">
<hr class="hrfooter" style="height:2px">
			<div class="store-wrapper">
				<?php dynamic_sidebar('footer-1'); ?>
			</div>
		</section>
		<?php
	}
	//Footer Widget
	if(is_active_sidebar('footer-2')){
		?>

		<section id="section-footer2" class="clear">
<hr style="height:2px">
			<div class="store-wrapper">
				<?php dynamic_sidebar('footer-2'); ?>
			</div>
		</section>
		<?php
	}
	?>
 	
</div><!-- #page -->



<div id="es-top"></div>
<?php wp_footer(); ?>

<div class = "comodo-footer">
<?php if (qtranxf_getLanguage() == 'en') { ?>
<div class= "text-comodo">
<a class = "tba-comodo">The Best Alpaca</a> sells high quality garments, we specialize in alpaca garments, 
however we also have a home section. 
Our best-selling garments are<a href="https://thebestalpaca.com/ponchos/" style="color:red"> ponchos</a>, but premium alpaca garments such as <a href="https://thebestalpaca.com/ruanas-capas/" style="color:red" >ruanas</a>, 
<a href="https://thebestalpaca.com/sweaters/" style="color:red">sweaters</a>, are highly valued because their premium quality.
We offer products made in Bolivia, the manufacture of garments are made by 
Bolivian artisans,who weave the garments by hand, and using instruments that help them knit.

We also sell accessories such as <a href="https://thebestalpaca.com/gloves/" style="color:red"> gloves, mittens</a>,<a href="https://thebestalpaca.com/scarfs/" style="color:red"> scarves</a>, <a href="https://thebestalpaca.com/shawls/" style="color:red">shawls</a>,<a href="https://thebestalpaca.com/2017/11/03/woman/" style="color:red"> slippers</a>,<a href="https://thebestalpaca.com/chullos/" style="color:red"> caps (chullos)</a>, 
all of them made with 100% alpaca wool.
And as for home accessories we have, <a href="https://thebestalpaca.com/home-2/" style="color:red">rugs and kitchen gloves</a>.

The quality of the alpaca wool of the Plateau is much higher than others due 
to the climatic conditions to which the animals are exposed.
That is why alpaca wool is five times warmer and lighter than other types of wool. 
Alpaca wool comes in 16 natural colors and an infinity of colors made with the same wool fiber.

<a href="https://thebestalpaca.com/" style="color:red">Visit our page</a> and review our products, you will love it.
</div>
<?php } elseif (qtranxf_getLanguage() == 'es') { ?>

<div class= "text-comodo">
<a class = "tba-comodo">The Best Alpaca</a> vende prendas de alta calidad, nos especializamos en prendas de alpaca,
sin embargo, también tenemos una sección de inicio.
Nuestras prendas más vendidas son <a href="https://thebestalpaca.com/ponchos/" style="color:red"> ponchos</a>, pero las prendas de alpaca premium como<a href="https://thebestalpac$<a href="https://thebestalpaca.com/sweaters/" style="color:red"> chompas</a>, son altamente valorados por su calidad premium.
Ofrecemos productos hechos en Bolivia, la fabricación de prendas está hecha por
Artesanos bolivianos, que tejen las prendas a mano y usan instrumentos que les ayudan a tejer.

Nosotros también ofrecemos accesorios como <a href="https://thebestalpaca.com/gloves/" style="color:red"> guantes,</a><a href="https://thebestalpaca.com/scarfs/" style="color:red">todos ellos hechos con 100% lana de alpaca.

Y en cuanto a los accesorios para el hogar que tenemos,<a href="https://thebestalpaca.com/home-2/" style="color:red">alfombras y guantes de cocina</a>.

La calidad de la lana de alpaca de la meseta es mucho mayor que otras debido
a las condiciones climáticas a las que están expuestos los animales.
Es por eso que la lana de alpaca es cinco veces más cálida y más ligera que otros tipos de lana.
La lana de alpaca viene en 16 colores naturales y una infinidad de colores hechos con la misma fibra de lana.



<a href="https://thebestalpaca.com/" style="color:red">Visita nuestra página</a> y revisa nuestros productos, amarás todos ellos!
</div>




<?php }  ?>

<div class = "img-comodo">
<script language="JavaScript" type="text/javascript">
TrustLogo("https://thebestalpaca.com/wp-content/uploads/2018/02/comodo_secure_seal_113x59_transp-1.png", "CL1", "none");
</script>
<a  href="https://ssl.comodo.com" id="comodoTL">Comodo SSL</a>

</div>

</div>

</body>
</html>
