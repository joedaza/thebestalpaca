<?php
/**
 * 8Store Lite functions and definitions
 *
 * @package 8Store Lite
 */

if ( ! function_exists( 'eightstore_lite_setup' ) ) :
/**
 * Sets up theme defaults and registers support for various WordPress features.
 *
 * Note that this function is hooked into the after_setup_theme hook, which
 * runs before the init hook. The init hook is too late for some features, such
 * as indicating support for post thumbnails.
 */
function eightstore_lite_setup() {
	/*
	 * Make theme available for translation.
	 * Translations can be filed in the /languages/ directory.
	 * If you're building a theme based on 8Store Lite, use a find and replace
	 * to change 'eightstore-lite' to the name of your theme in all the template files
	 */
	load_theme_textdomain( 'eightstore-lite', get_template_directory() . '/languages' );

	// Add default posts and comments RSS feed links to head.
	add_theme_support( 'automatic-feed-links' );

	/*
	 * Let WordPress manage the document title.
	 * By adding theme support, we declare that this theme does not use a
	 * hard-coded <title> tag in the document head, and expect WordPress to
	 * provide it for us.
	 */
	add_theme_support( 'title-tag' );

	/*
	 * Enable support for Post Thumbnails on posts and pages.
	 *
	 * @link http://codex.wordpress.org/Function_Reference/add_theme_support#Post_Thumbnails
	 */
	add_theme_support( 'post-thumbnails' );

	add_image_size('eightstore-promo-small', 520, 260, true);
	add_image_size('eightstore-promo-large', 520, 520, true);
	add_image_size('eightstore-blog-image', 290, 260, true);
	add_image_size('eightstore-testimonial-image', 70, 70, true);

	// This theme uses wp_nav_menu() in one location.
	register_nav_menus( array(
		'primary' => esc_html__( 'Primary Menu', 'eightstore-lite' ),
		) );

	/*
	 * Switch default core markup for search form, comment form, and comments
	 * to output valid HTML5.
	 */
	add_theme_support( 'html5', array(
		'search-form',
		'comment-form',
		'comment-list',
		'gallery',
		'caption',
		) );

	/*
	 * Enable support for Post Formats.
	 * See http://codex.wordpress.org/Post_Formats
	 */
	add_theme_support( 'post-formats', array(
		'aside',
		'image',
		'video',
		'quote',
		'link',
		) );

	// Set up the WordPress core custom background feature.
	add_theme_support( 'custom-background', apply_filters( 'eightstore_lite_custom_background_args', array(
		'default-color' => 'ffffff',
		'default-image' => '',
		) ) );

	// Add theme support for selective refresh for widgets.
	add_theme_support( 'customize-selective-refresh-widgets' );

	add_editor_style( array( 'css/editor-style.css') );
}
endif; // eightstore_lite_setup
add_action( 'after_setup_theme', 'eightstore_lite_setup' );

/**
 * Set the content width in pixels, based on the theme's design and stylesheet.
 *
 * Priority 0 to make it available to lower priority callbacks.
 *
 * @global int $content_width
 */
function eightstore_lite_content_width() {
	$GLOBALS['content_width'] = apply_filters( 'eightstore_lite_content_width', 640 );
}
add_action( 'after_setup_theme', 'eightstore_lite_content_width', 0 );

/**
 * Register widget area.
 *
 * @link http://codex.wordpress.org/Function_Reference/register_sidebar
 */
function eightstore_lite_widgets_init() {
	register_sidebar( array(
		'name'          => esc_html__( 'Sidebar', 'eightstore-lite' ),
		'id'            => 'sidebar-1',
		'description'   => 'Sidebar',
		'before_widget' => '<aside id="%1$s" class="widget %2$s">',
		'after_widget'  => '</aside>',
		'before_title'  => '<h1 class="widget-title">',
		'after_title'   => '</h1>',
		) );
	register_sidebar( array(
		'name'          => __( 'Shop Sidebar', 'eightstore-lite' ),
		'id'            => 'shop',
		'description'   => '',
		'before_widget' => '<div id="%1$s" class="%2$s '.eightstore_lite_count_widgets( 'shop' ).'">',
		'after_widget'  => '</div>',
		'before_title'  => '<span class="widget-title">',
		'after_title'   => '</span>',
		) );

	register_sidebar( array(
		'name'          => esc_html__( 'Language Translator', 'eightstore-lite' ),
		'id'            => 'eightstore-lite-language-option',
		'description'   => 'Add Plugin and place its widget here.',
		'before_widget' => '<aside id="%1$s" class="widget %2$s">',
		'after_widget'  => '</aside>',
		) );
	register_sidebar( array(
		'name'          => esc_html__( 'Product Widget 1', 'eightstore-lite' ),
		'id'            => 'widget-product-1',
		'description'   => 'Show a slider of product',
		'before_widget' => '<aside id="%1$s" class="widget %2$s">',
		'after_widget'  => '</aside>',
		) );
	register_sidebar( array(
		'name'          => esc_html__( 'Promo Widget 1', 'eightstore-lite' ),
		'id'            => 'widget-promo-1',
		'description'   => 'Show banner or text or some call to action',
		'before_widget' => '<aside id="%1$s" class="widget %2$s '.eightstore_lite_count_widgets('widget-promo-1').'">',
		'after_widget'  => '</aside>',
		) );
	register_sidebar( array(
		'name'          => esc_html__( 'Category Widget 1', 'eightstore-lite' ),
		'id'            => 'widget-category-1',
		'description'   => 'Show a slider with category details and product of it.',
		'before_widget' => '<aside id="%1$s" class="widget %2$s">',
		'after_widget'  => '</aside>',
		) );
	register_sidebar( array(
		'name'          => esc_html__( 'Promo Widget 2', 'eightstore-lite' ),
		'id'            => 'widget-promo-2',
		'description'   => 'Show banner or text or some call to action',
		'before_widget' => '<aside id="%1$s" class="widget %2$s">',
		'after_widget'  => '</aside>',
		) );
	register_sidebar( array(
		'name'          => esc_html__( 'Category Widget 2', 'eightstore-lite' ),
		'id'            => 'widget-category-2',
		'description'   => 'Show a slider with category details and product of it.',
		'before_widget' => '<aside id="%1$s" class="widget %2$s">',
		'after_widget'  => '</aside>',
		) );
	register_sidebar( array(
		'name'          => esc_html__( 'Promo Widget 3', 'eightstore-lite' ),
		'id'            => 'widget-promo-3',
		'description'   => 'Show banner or text or some call to action',
		'before_widget' => '<aside id="%1$s" class="widget %2$s">',
		'after_widget'  => '</aside>',
		) );
	register_sidebar( array(
		'name'          => esc_html__( 'Product Widget 2', 'eightstore-lite' ),
		'id'            => 'widget-product-2',
		'description'   => 'Show a slider of product',
		'before_widget' => '<aside id="%1$s" class="widget %2$s">',
		'after_widget'  => '</aside>',
		) );	
	
	register_sidebar( array(
		'name'          => esc_html__( 'Promo Widget 4', 'eightstore-lite' ),
		'id'            => 'widget-promo-4',
		'description'   => 'Show banner or text or some call to action',
		'before_widget' => '<aside id="%1$s" class="widget %2$s">',
		'after_widget'  => '</aside>',
		) );
	
	register_sidebar( array(
		'name'          => __( 'Sidebar - Left', 'eightstore-lite' ),
		'id'            => 'sidebar-left',
		'description'   => '',
		'before_widget' => '<aside id="%1$s" class="widget %2$s">',
		'after_widget'  => '</aside>',
		) );
	
	register_sidebar( array(
		'name'          => __( 'Sidebar - Right', 'eightstore-lite' ),
		'id'            => 'sidebar-right',
		'description'   => '',
		'before_widget' => '<aside id="%1$s" class="widget %2$s ">',
		'after_widget'  => '</aside>',
		'before_title'  => '<h1 class="widget-title">',
		'after_title'   => '</h1>',
		) );
	register_sidebar( array(
		'name'          => esc_html__( 'Top Footer Widgets', 'eightstore-lite' ),
		'id'            => 'footer-1',
		'description'   => '',
		'before_widget' => '<aside id="%1$s" class="top-footer-widget %2$s">',
		'after_widget'  => '</aside>',
		'before_title'  => '<h2 class="footer-widget-title">',
		'after_title'   => '</h2>',
		) );
	register_sidebar( array(
		'name'          => esc_html__( 'Footer Widgets', 'eightstore-lite' ),
		'id'            => 'footer-2',
		'description'   => '',
		'before_widget' => '<aside id="%1$s" class="main-footer-widget %2$s">',
		'after_widget'  => '</aside>',
		'before_title'  => '<h2 class="footer-widget-title">',
		'after_title'   => '</h2>',
		) );
}
add_action( 'widgets_init', 'eightstore_lite_widgets_init' );

/**
 * Enqueue scripts and styles.
 */
function eightstore_lite_scripts() {

	$font_args = array(
		'family' => 'Open+Sans:400,600,700,300|Oswald:400,700,300|Dosis:400,300,500,600,700|Lato:400,300,700,900',
		);
	wp_enqueue_style('eightstore-google-fonts', add_query_arg($font_args, "//fonts.googleapis.com/css"));
	
add_filter( 'woocommerce_attribute', 'make_product_atts_linkable', 10, 3 );
	wp_enqueue_style('font-awesome', get_template_directory_uri() . '/css/font-awesome.min.css' );

	wp_enqueue_style( 'eightstore-animate', get_template_directory_uri() . '/css/animate.css');
	
	wp_enqueue_style( 'eightstore-slick', get_template_directory_uri() . '/css/slick.css');

	wp_enqueue_style( 'eightstore-fancybox', get_template_directory_uri() . '/css/fancybox.css');

	wp_enqueue_style( 'eightstore-custom-scrollcss', get_template_directory_uri() . '/css/jquery.mCustomScrollbar.css');

	wp_enqueue_style( 'eightstore-style', get_stylesheet_uri() );

	//check if responsive mode is enabled.
	if(get_theme_mod('is_mode_responsive')!='1'){
		wp_enqueue_style( 'eightstore-responsive', get_template_directory_uri() . '/css/responsive.css');
	}

	wp_enqueue_script( 'eightstore-mousewheel', get_template_directory_uri() . '/js/jquery.mousewheel-3.0.4.pack.js', array('jquery'), '3.0.4', true );
	wp_enqueue_script( 'eightstore-fancybox', get_template_directory_uri() . '/js/jquery.fancybox-1.3.4.js', array('jquery'), '1.3.4', true );

	wp_enqueue_script( 'eightstore-wow', get_template_directory_uri() . '/js/wow.min.js',array(),'1.1.2',true);

	wp_enqueue_script( 'eightstore-slick', get_template_directory_uri() . '/js/slick.js', array('jquery'), '1.5.0', true );


	wp_enqueue_script( 'eightstore-navigation', get_template_directory_uri() . '/js/navigation.js', array(), '20120206', true );

	wp_enqueue_script( 'eightstore-custom-scrolljs', get_template_directory_uri() . '/js/jquery.mCustomScrollbar.concat.min.js', array(), '20130115', true );
	
	wp_enqueue_script( 'eightstore-skip-link-focus-fix', get_template_directory_uri() . '/js/skip-link-focus-fix.js', array(), '20130115', true );

	if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
		wp_enqueue_script( 'comment-reply' );
	}

	wp_enqueue_script('eightstore-custom-scripts', get_template_directory_uri() . '/js/custom-scripts.js', array(), '20150611', true );
}
add_action( 'wp_enqueue_scripts', 'eightstore_lite_scripts' );

/**
 * Implement the Custom Header feature.
 */
require get_template_directory() . '/inc/custom-header.php';

/**
 * Custom template tags for this theme.
 */
require get_template_directory() . '/inc/template-tags.php';

/**
 * Custom functions that act independently of the theme templates.
 */
require get_template_directory() . '/inc/extras.php';

/**
 * Customizer additions.
 */
require get_template_directory() . '/inc/customizer.php';

/**
 * Load Jetpack compatibility file.
 */
require get_template_directory() . '/inc/jetpack.php';

/**
 * Load Custom functions file.
 */
require get_template_directory() . '/inc/eightstore-functions.php';
/**
 * Custom Customizer additions.
 */
require get_template_directory() . '/inc/eightstore-customizer.php';

/**
 * Custom Sanitizer additions.
 */
require get_template_directory() . '/inc/eightstore-sanitizer.php';
/**
 * Custom Metabox Additions.
 */
require get_template_directory() . '/inc/eightstore-metabox.php';

/**
 * Custom Typography dropdown
 */
require get_template_directory() . '/inc/typography-dropdown.php';
/**
 * Custom Control Type additions.
 */
require get_template_directory() . '/inc/controls/custom-switch.php';
require get_template_directory() . '/inc/controls/custom-chooseimage.php';
require get_template_directory() . '/inc/controls/category-dropdown.php';

/**
 * Custom Widget Types additions.
 */
require get_template_directory() . '/inc/widgets/es-widgets.php';
/**
 * Load Custom Styles
 */
require get_template_directory() . '/css/config-styles.php';
/**
 * Load support Information
 */
require get_template_directory() . '/welcome/eightstore_lite_about.php';


/* Function para agregar texto luego del precio 

function mostrar_texto_desp_precio($priceoff) {

   $priceoff .=  ' (45% OFF)';
  
   return $priceoff;

}


add_filter( 'woocommerce_get_price_html', 'mostrar_texto_desp_precio' );
add_filter( 'woocommerce_cart_item_price', 'mostrar_texto_desp_precio' );
*/


/*gallery images*/

/*
add_action( 'after_setup_theme', 'layerswoo_theme_setup' );
function layerswoo_theme_setup() {
   add_theme_support( 'wc-product-gallery-zoom' );
   add_theme_support( 'wc-product-gallery-lightbox' );
   add_theme_support( 'wc-product-gallery-slider' );
}
*/

/*Deshabilitar funcion de galeria de img en woocommerce*/

add_action( 'after_setup_theme', 'remove_woo_features', 999 );
function remove_woo_features(){
remove_theme_support( 'wc-product-gallery-slider' );
remove_theme_support( 'wc-product-gallery-zoom' );
remove_theme_support( 'wc-product-gallery-lightbox' );
}



/* login */
add_action( 'template_redirect', 'redirect_to_specific_page');

function redirect_to_specific_page() {

if ( is_page('checkout') && ! is_user_logged_in() ) {

wp_redirect( 'https://thebestalpaca.com/login/', 301); 


  exit;
add_filter( 'woocommerce_attribute', 'make_product_atts_linkable', 10, 3 );
    }

}


/*add link in product attribute*/



/**
 * Register term fields
 */
add_action( 'init', 'register_attributes_url_meta' );
function register_attributes_url_meta() {
        $attributes = wc_get_attribute_taxonomies();

        foreach ( $attributes as $tax ) {
            $name = wc_attribute_taxonomy_name( $tax->attribute_name );

            add_action( $name . '_add_form_fields', 'add_attribute_url_meta_field' );
            add_action( $name . '_edit_form_fields', 'edit_attribute_url_meta_field', 10 );
            add_action( 'edit_' . $name, 'save_attribute_url' );
            add_action( 'create_' . $name, 'save_attribute_url' );
        }
}

/**
 * Add term fields form
 */
function add_attribute_url_meta_field() {

    wp_nonce_field( basename( __FILE__ ), 'attrbute_url_meta_nonce' );
    ?>

    <div class="form-field">
        <label for="attribute_url"><?php _e( 'URL', 'domain' ); ?></label>
        <input type="url" name="attribute_url" id="attribute_url" value="" />
    </div>
    <?php
}

/**
 * Edit term fields form
 */
function edit_attribute_url_meta_field( $term ) {

    $url = get_term_meta( $term->term_id, 'attribute_url', true );
    wp_nonce_field( basename( __FILE__ ), 'attrbute_url_meta_nonce' );
    ?>
    <tr class="form-field">
        <th scope="row" valign="top"><label for="attribute_url"><?php _e( 'URL', 'domain' ); ?></label></th>
        <td>
            <input type="url" name="attribute_url" id="attribute_url" value="<?php echo esc_url( $url ); ?>" />
        </td>
    </tr>
    <?php
}

/**
 * Save term fields
 */
function save_attribute_url( $term_id ) {
    if ( ! isset( $_POST['attribute_url'] ) || ! wp_verify_nonce( $_POST['attrbute_url_meta_nonce'], basename( __FILE__ ) ) ) {
        return;
    }

add_filter( 'woocommerce_attribute', 'make_product_atts_linkable', 10, 3 );
    $old_url = get_term_meta( $term_id, 'attribute_url', true );
    $new_url = esc_url( $_POST['attribute_url'] );


    if ( ! empty( $old_url ) && $new_url === '' ) {
        delete_term_meta( $term_id, 'attribute_url' );
    } else if ( $old_url !== $new_url ) {
        update_term_meta( $term_id, 'attribute_url', $new_url, $old_url );
    }
}

/**
 * Show term URL
 */


add_filter( 'woocommerce_attribute', 'make_product_atts_linkable', 10, 3 );

function make_product_atts_linkable( $text, $attribute, $values ) {
    $new_values = array();
    foreach ( $values as $value ) {

        if ( $attribute['is_taxonomy'] ) {
            $term = get_term_by( 'name', $value, $attribute['name'] );
            $url = get_term_meta( $term->term_id, 'attribute_url', true );

            if ( ! empty( $url ) ) {
                $val = '<a href="' . esc_url( $url ) . '" title="' . esc_attr( $value ) . '">' . $value . '</a>';
                array_push( $new_values, $val );
            } else {
                array_push( $new_values, $value );
            }
        } else {
            $matched = preg_match_all( "/\[([^\]]+)\]\(([^)]+)\)/", $value, $matches );

            if ( $matched && count( $matches ) == 3 ) {
                $val = '<a href="' . esc_url( $matches[2][0] ) . '" title="' . esc_attr( $matches[1][0] ) . '">' . sanitize_text_field( $matches[1][0] ) . '</a>';
add_filter( 'woocommerce_attribute', 'make_product_atts_linkable', 10, 3 );
                array_push( $new_values, $val );
            } else {
                array_push( $new_values, $value );
            }
        }
    }

    $text = implode( ', ', $new_values );
    return $text;
}


add_filter( 'add_to_cart_url', 'wpa104168_tadd_special_payment_link' );

function wpa104168_tadd_special_payment_link( $link ) {
    global $product;

    // If the stock quantity is greater than 50, modify the link
    if( $product->get_stock_quantity() > 50 ) {
        $link = 'http://wordpress.org/';  
        $product->product_type = 'modified';  
    }

    return $link;
}

/*Orden del additional information*/


remove_action( 'woocommerce_after_single_product_summary', 'woocommerce_output_product_data_tabs', 10 );
add_action( 'woocommerce_single_product_summary', 'woocommerce_output_product_data_tabs', 80 );


add_filter( 'woocommerce_product_tabs', 'woo_reorder_tabs', 98 );
function woo_reorder_tabs( $tabs ) {
	
	$tabs['additional_information']['priority'] = 5;	// Additional information third

	return $tabs;
}

/*login falla se direcciona a otra pagina*/

add_action( 'wp_login_failed', 'front_end_login_fail' );
function front_end_login_fail( $username ) {

$_SESSION['uname'] =  $username;
// Getting URL of the login page
$referrer = $_SERVER['HTTP_REFERER'];    
$login_failed_error_codes = array( 'empty_password', 'empty_email', 'invalid_email', 'invalidcombo', 'empty_username', 'invalid_username', 'incorrect_password' );

// if there's a valid referrer, and it's not the default log-in screen
if( !empty( $referrer ) && !strstr( $referrer,'wp-login' ) && !strstr( $referrer,'wp-admin' ) ) {
    //wp_redirect( get_permalink( 93 ) . "?login=failed" ); 
    wp_redirect( 'https://thebestalpaca.com/login-error/' . "?login=failed" );
    exit;
}

}


/*note after category*/

add_action('woocommerce_product_meta_end','add_pet_info' );
function add_pet_info($pet_info) {
    _e( "[:en]<b>Note:</b>&nbsp;&nbsp; As the garments are unique, the product can change with the regard to the description and photo<br><br><br>[:es]<b>Nota:</b> &nbsp;&nbsp; Como las prendas son únicas, el producto puede cambiar con respecto a la descripción y la foto<br><br><br>[:] ", "your-textdomain" );
}




