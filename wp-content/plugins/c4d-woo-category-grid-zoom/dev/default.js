(function($){
	"use strict";
	$(document).ready(function(){
		var list = $('.woocommerce ul.products'),
		cols = 4;
		$('body').on('click', '.c4d-woo-cgz .zoom-button', function(){
			if ($(this).hasClass('zoom-in')) {
				if (cols > 1) {
					list.attr('data-cols', cols - 1);	
					cols--;
				}
			} else {
				if (cols < 12) {
					list.attr('data-cols', cols + 1);					
					cols++;
				}
			}
		});
	});
})(jQuery);